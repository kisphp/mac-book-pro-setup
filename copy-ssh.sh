#!/bin/bash

cp id_rsa $HOME/.ssh/ 
cp id_rsa.pub $HOME/.ssh/
chmod 0600 $HOME/.ssh/id_rsa*
