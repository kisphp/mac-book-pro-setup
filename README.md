## Before installation

Open `ansible/config.yml` file and change configuration parameters according to your preferences:

**example:**
```
# this will assemble email: johndoe@gmail.com
email_domain: "gmail.com"
email_username: "johndoe"

# Username to be used by git, other way it will use the system user
git_username: "John Doe"

```

## Installation

### Run pre installation script (only first time)
```
./setup.sh
```

### Run ansible playbook to install all needed applications
```
cd ansible
ansible-playbook -i env/local/main.ini setup.yml
```
