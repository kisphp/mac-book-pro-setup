#!/bin/bash

if [ ! -f /usr/local/bin/brew ]; then
	# install homebrew
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# install ansible
brew install ansible

# install iterm2
brew install Caskroom/cask/iterm2

###!/bin/bash

#brew tap homebrew/dupes
#brew tap homebrew/versions
#brew tap homebrew/homebrew-php
#brew unlink php56
#brew install php70

#sudo mv /usr/bin/php /usr/bin/php.bkp
#sudo ln -s /usr/local/Cellar/php70/7.0.15_8/bin/php /usr/bin/php

#$(which php) --version
